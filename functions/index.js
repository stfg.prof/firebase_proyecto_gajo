const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp(functions.config.firebase);
const db = admin.firestore();
const listaLocaciones = db.doc("listaLocaciones/listaGeoPoints");

//IMPORTANTE LEER https://microeducate.tech/how-to-perform-an-arrayunion-and-an-arrayremove-in-a-single-query-using-firebases-firestore/

/*
async function actualizarListaGajos(actualizar, elminar = null) {
  if (!!elminar) {
    return await listaLocaciones.update({
      listaGeoPoints: admin.firestore.FieldValue.arrayRemove(elminar),
      listaGeoPoints: admin.firestore.FieldValue.arrayUnion(actualizar),
    });
    // return await
    // listaLocaciones.update({
    //   listaGeoPoints: admin.firestore.FieldValue.arrayRemove(elminar),
    // }).then(()=>{ listaLocaciones.update({
    //   listaGeoPoints: admin.firestore.FieldValue.arrayUnion(actualizar),
    // })});
  } else {
    await listaLocaciones.update({
      listaGeoPoints: admin.firestore.FieldValue.arrayUnion(actualizar),
      //   });
    });
  }
}
*/

//agregar cambio de storage
exports.onCreateGajo = functions.firestore
  .document(`/gajos/{documentId}`)
  .onCreate((snap, context) => {
    const doc = snap.data();
    const geoLoc = doc.geoLoc;

    //luego ver si hago otro tipo de lista, con un dict o algo para emparejar loc con url o directamente hago el fetch desde una consulta
    // a la vez si hay gran cantidad de gajos voy a tener q modificarla para que creer listras de n° elementos
    return listaLocaciones.update({
      listaGeoPoints: admin.firestore.FieldValue.arrayUnion(geoLoc),
      //   });
    });
  });

//Hacer un onUpdate que al cambio elimine la img y la geo loc agregando la nueva
//una func delete?

// https://firebase.google.com/docs/functions/firestore-events#trigger_a_function_when_a_document_is_updated

exports.onUpdateGajo = functions.firestore
  .document("gajos/{userId}")
  .onUpdate(async (change, context) => {
    const newValueGeoloc = change.after.data().geoLoc;
    const previousValueGeoloc = change.before.data().geoLoc;

    if (newValueGeoloc != previousValueGeoloc) {
      return await listaLocaciones
        .update({
          listaGeoPoints:
            admin.firestore.FieldValue.arrayRemove(previousValueGeoloc),
        })
        .then(() => {
          listaLocaciones.update({
            listaGeoPoints:
              admin.firestore.FieldValue.arrayUnion(newValueGeoloc),
          });
        });
    } else return 0;
  });
